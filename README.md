# ionic-test

To run locally:
* clone the repo
* cd to the repo directory
* npm install
* npm run ionic:serve --copy copy.config.js


## Data
We use [this Google Sheet](https://docs.google.com/spreadsheets/d/15-60RpIqkM2nev4S0UNt7h5M5mhuvegPFO8X-TU8rKg/edit?usp=sharing) 
as our data curating source which feeds the data ultimately coming to the website from a PostgreSQL database via
a [REST API](https://parks.api.codefornrv.org).