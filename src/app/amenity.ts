export class Amenity {
    public Name: string;
    public Id: number;

    public constructor(init?:Partial<Amenity>) {
        Object.assign(this, init);
    }
}

export interface JsonAmenity {
    id: number;
    amenity_name: string;
    description: string;
    alternate_names: Array<string>;
}

export interface JsonAmenitiesParks {
    id: number;
    park_id: number;
    amenity_type_id: number;
}