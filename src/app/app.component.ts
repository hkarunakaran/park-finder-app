import { Component, ViewChild, NgZone } from '@angular/core';
import { Nav, Platform, Events, LoadingController, Loading } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { IData, APIData } from './data';
import { IFilter } from './filter';
import { AboutPage } from '../pages/about/about';
import { HttpClient } from '@angular/common/http';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  pages: Array<{title: string, component: any}>;

  data: IData;

  filter: IFilter;

  sort: string;

  listActive: boolean;

  locationKnown: boolean;

  hideExtra: boolean;

  pageIcons = {
    'Map': 'navigate',
    'List': 'list-box',
    'About nrvparks.com': 'help'
  }

  constructor(public zone: NgZone, public platform: Platform, 
    public statusBar: StatusBar, public splashScreen: SplashScreen, public events: Events, 
    private http: HttpClient, private loadingController: LoadingController) {

    let loader = loadingController.create({
      content: "Loading data..."
    });  
    loader.present();
    this.data = new APIData();
    this.data.Initialize(http, () => this.initializeApp(this, loader), () => this.initializationError(loader));
  }

  initializationError(loader: Loading) {
    loader.dismiss();
    alert("Could not load data.  The site may be under maintenance.  Try again later.");
  }

  initializeApp(context: MyApp, loader: Loading) {
    context.pages = [
      { title: 'Map', component: HomePage },
      { title: 'List', component: ListPage },
      { title: 'About nrvparks.com', component: AboutPage }
    ];

    context.sort = "alphabetical";

    context.applySort(context.sort);

    context.listActive = false;

    context.hideExtra = true;

    context.locationKnown = false;

    context.zone = new NgZone({ enableLongStackTrace: false });

    context.platform.ready().then(() => {
      context.statusBar.styleDefault();
      context.splashScreen.hide();
      context.nav.push(context.pages[0].component, {data: context.data});
      // fix meta tag to allow scaling and zooming
      let metaElement = document.querySelector("meta[name='viewport']");
      metaElement.setAttribute('content', 'viewport-fit=cover, width=device-width, initial-scale=1.0');
    });

    loader.dismiss();
  }

  requestLocation() {
    let loader = this.loadingController.create({
      content: "Getting your location..."
    });  
    loader.present();
    navigator.geolocation
      .getCurrentPosition(
        (position) => {
          loader.dismiss();
          this.events.publish('userlocation:updated', position);
          this.locationKnown = true;
          this.applySort('distance');
        },
        (error) => {
          loader.dismiss();
          alert("Could not get your location: " + error.message);
        },
        {timeout: 10000}
      );
  }

  applyFilter(filter) {
    this.filter = filter;
    this.data.FilterUpdated();
    this.events.publish('filter:updated', filter);
  }

  applySort(sortOrder) {
    this.sort = sortOrder;
    this.data.SortParks(sortOrder);
  }

  openPage(page) {
    if(page.component == this.nav.getActive().component) { return; }
    this.listActive = page.component == ListPage;
    this.nav.setRoot(page.component, {data: this.data, filter: this.filter, sort: this.sort});
  }

  isPageActive(page) {
    return this.nav.getActive() && this.nav.getActive().component === page.component;
  }

  toggleHideExtra() {
    this.hideExtra = !this.hideExtra;
  }

  reset() {
    this.hideExtra = true;
    this.data.ClearFilters();
    this.events.publish('filter:reset');
  }
}

@Component({
  selector: '<park-details-link>',
  template: 'hello'
})
export class ParkDetailsLink {
  constructor() {

  }
}
