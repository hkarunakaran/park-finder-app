import {Amenity} from './amenity'
import {Park} from './park'

export interface IFilter {
    Filter(parks: Array<Park>): Array<Park>;
    IsActive: boolean;
    Name: string;
}

export class SearchFilter implements IFilter {
    IsActive: boolean = false;
    Name: string = "Search";

    constructor(public Term: string) { }

    public Filter(parks: Array<Park>): Array<Park> {
        return parks.filter(p => 
            (p.Name != null && p.Name.toLowerCase().includes(this.Term.toLowerCase())) || 
            (p.Description != null && p.Description.toLowerCase().includes(this.Term.toLowerCase())) || 
            (p.Address != null && p.Address.toLowerCase().includes(this.Term.toLowerCase())) || 
            (p.URL != null && p.URL.toLowerCase().includes(this.Term.toLowerCase())) || 
            (p.AlternateNames != null && p.AlternateNames.filter(n => n.includes(this.Term.toLowerCase())).length > 0) || 
            (p.Amenities != null && p.Amenities.map(a => a.Name.toLowerCase()).filter(n => n.includes(this.Term.toLowerCase())).length > 0));
    }
}

export class AmenityFilter implements IFilter {
    Amenity: Amenity;
    IsActive: boolean = false;
    Name: string;

    constructor(amenity: Amenity) {
        this.Amenity = amenity;
        this.Name = amenity.Name;
    }

    public Filter(parks: Array<Park>): Array<Park> {
        return parks.filter(park => park.Amenities.filter(amenity => amenity.Id == this.Amenity.Id).length > 0);
    }
}

export class OrFilter implements IFilter {
    Filters: Array<IFilter>;
    IsActive: boolean = false;
    Name: string;

    constructor(amenities: Array<Amenity>, name: string) {
        this.Filters = amenities.map(x => new AmenityFilter(x));
        this.Name = name;
    }

    public Filter(parks: Array<Park>): Array<Park> {
        var valid = new Set<Park>();
        for(let filter of this.Filters) {
            for(let park of filter.Filter(parks)) {
                valid.add(park);
            }
        }
        return Array.from(valid);
    }
}

export class NoFilter implements IFilter {
    IsActive: boolean = false;
    Name: string = "All";

    public Filter(parks: Array<Park>): Array<Park> {
        return parks;
    }
}