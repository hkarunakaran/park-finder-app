import { Component } from '@angular/core';
import { Events, NavParams } from 'ionic-angular';
import { Platform } from 'ionic-angular/platform/platform';
import { NRVMap } from '../../app/nrvmap'
import { Park } from '../../app/park'
import { IData } from '../../app/data';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  public Map: NRVMap;
  public Data: IData;
  public SearchStr: string;

  constructor(public navParams: NavParams, platform: Platform, public events: Events) {
    events.subscribe('userlocation:updated', (position) => {
      this.Map.SetUserLocation(position);
      this.Data.SetUserLocation(position.coords.latitude, position.coords.longitude);
    });

    events.subscribe('filter:updated', () => {
      this.UpdateMarkers();
    });

    events.subscribe('filter:reset', () => {
      this.SearchStr = "";
      this.UpdateMarkers();
      // todo: find source of asynchronicity
      setTimeout(() => this.Map.FitBounds(), 500);
    });

    this.Data = navParams.get('data');
  }

  public InitializeData() {

    for(let park of this.Data.Parks) {
      this.AddPark(park);
    }

    this.UpdateMarkers();

    this.Map.FitBounds();
  }

  public AddPark(park: Park) {
    this.Map.AddPark(park);
  }

  public UpdateMarkers() {
    for(let park of this.Data.Parks) {
      if(park.Visible) {
        this.Map.ShowMarker(park.Id);
      } else {
        this.Map.HideMarker(park.Id);
      }
    }
  }

  ionViewDidLoad() {
    var layerURL = 'https://api.mapbox.com/styles/v1/nealf/cj4a7oll139od2spe6ckgufjq/tiles/256/{z}/{x}/{y}?access_token=pk.eyJ1IjoibmVhbGYiLCJhIjoiNmM4MGQ3M2UzNmVlMTY0OWNmZDhiZjk0YWZlYzQ4OTYifQ.VEiV66Tl7sjD5n-bDLjbhw';
    var attribution = '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors';
    this.Map = new NRVMap('mapid', layerURL, attribution, 37.129852, -80.408939);
    this.InitializeData();
  }

  public SetActivePark(park: Park) {
    this.Data.ActivePark = park;
  }

  public Search() {
    var filter = this.Data.Search(this.SearchStr);
    this.events.publish('filter:updated', filter);
  }

  public ClearSearch() {
    this.SearchStr = "";
    this.Search();
  }
}
